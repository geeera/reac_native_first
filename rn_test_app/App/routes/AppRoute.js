import React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from '@react-navigation/stack';

import {AppProvider} from '../store/store'
import {MainScreen} from "../screens/MainScreen";
import {DriverScreen} from '../screens/DriverScreen';
import {DriverStandingsScreen} from '../screens/DriverStandingsScreen';


export const AppRoute = () => {
    const Stack = createStackNavigator()
    return(
        <AppProvider>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Main" screenOptions={{
                    headerStyle: {
                        backgroundColor: "#ff6666"
                    },
                    headerTintColor: "#fff",
                    headerTitleStyle: {
                        textTransform: 'capitalize'
                    }
                }}>
                    <Stack.Screen name='Main' component={MainScreen} />
                    <Stack.Screen name='Driver'
                                  options={({route}) => ({title: route.params.driverID || 'Driver Name'})}
                                  component={DriverScreen} />
                    <Stack.Screen name='Driver Standing' component={DriverStandingsScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        </AppProvider>
    )
}
