import React from 'react';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';

import {selectsReducer} from './reducers/selectsReducer';
import {filterTableReducer} from './reducers/filterTableReducer';
import {driverReducer} from './reducers/driverReducer';
import {driverStandingReducer} from './reducers/driverStandingReducer';
import {errorReducer} from './reducers/errorReducer';

const rootReducer = combineReducers({
    selects: selectsReducer,
    table: filterTableReducer,
    driver: driverReducer,
    driverStanding: driverStandingReducer,
    error: errorReducer
})

export const store = createStore(rootReducer, applyMiddleware(thunk))

export const AppProvider = ({children}) => {
    return(
        <Provider store={store}>
            {children}
        </Provider>
    )
}
