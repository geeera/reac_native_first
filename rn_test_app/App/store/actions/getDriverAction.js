import {CLEAN_ERR, GET_DRIVER, GET_ERR} from '../types';
import {ApiRequest} from '../apiRequest';

export const getDriver = (payload) => async dispatch => {
    try {
        dispatch({type: CLEAN_ERR})
        const Driver = await ApiRequest.getDriver(payload)
        dispatch({
            type: GET_DRIVER,
            payload: Driver
        })
    } catch (e) {
        dispatch({
            type: GET_ERR,
            payload: 'Can`t get driver.'
        })
    }
}
