import {CLEAN_ERR, GET_ERR} from '../types';

export const asyncAction = async (dispatch, config) => {
    try {
        dispatch({type: CLEAN_ERR})
        const res = await fetch(config.url, {
            method: config.method,
            headers: {'Content-Type': 'application/json'}
        });
        const {MRData} = await res.json();
        const data = [...MRData.RaceTable.Races]
        const newData = data.map((item) => item[config.needData])
        const filterData = newData.filter((el, _, arr) => {
            return el !== arr[_ + 1];
        })

        dispatch({
            type: config.action,
            payload: filterData
        });
    } catch (e) {
        dispatch({
            type: GET_ERR,
            payload: 'Can`t load the info.'
        })
        return e
    }
}
