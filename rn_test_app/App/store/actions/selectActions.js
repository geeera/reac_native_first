import {ApiRequest} from '../apiRequest';

export const changeSeason = () => {
    return async (dispatch) => await ApiRequest.changeSeason(dispatch)
}

export const changeRound = (payload) => {
    return async (dispatch) => await ApiRequest.changeRound(dispatch, payload)
}
