import {CLEAN_ERR, GET_DRIVER_Standing, GET_ERR} from '../types';
import {ApiRequest} from '../apiRequest';

export const getDriverStandingAction = ({year, round}) => async dispatch => {
    try {
        dispatch({type: CLEAN_ERR})
        const Drivers = await ApiRequest.getDriverStanding(year, round)
        dispatch({
            type: GET_DRIVER_Standing,
            payload: Drivers
        })
    } catch (e) {
        dispatch({
            type: GET_ERR,
            payload: 'Can`t get driver standing list.'
        })
    }
}
