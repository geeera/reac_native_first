import {CHANGE_PAGE, CLEAN_ERR, GET_DRIVERS, GET_ERR} from '../types';
import {ApiRequest} from '../apiRequest';

export const filterTableAction = (payload) => async dispatch => {
    try {
        dispatch({type: CLEAN_ERR})
        const data = await ApiRequest.filterTable(payload)
        if (data.length) {
            dispatch({type: GET_DRIVERS, payload: data})
            dispatch({type: CHANGE_PAGE, payload: payload.page || 1})
        }
    } catch (e) {
        dispatch({
            type: GET_ERR,
            payload: 'Can`t get driver list.'
        })
        dispatch({type: GET_DRIVERS, payload: []})
        dispatch({type: CHANGE_PAGE, payload: 1})
    }
}
