import {GET_DRIVER_Standing} from '../types';

const initialState = {
    table: {}
}

export const driverStandingReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DRIVER_Standing:
            return {
                ...state,
                table: action.payload
            }
        default:
            return state
    }
}
