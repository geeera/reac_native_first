import {CLEAN_ERR, GET_ERR} from '../types';

const initState = {
    errMessage: null
}
export const errorReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_ERR:
            return {
                ...state,
                errMessage: action.payload
            }
        case CLEAN_ERR:
            return {
                ...state,
                errMessage: null
            }
        default:
            return state
    }
}
