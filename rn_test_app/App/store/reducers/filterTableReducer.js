import {CHANGE_PAGE, GET_DRIVERS} from '../types';

const initState = {
    drivers: [],
    page: 0
}

export const filterTableReducer = (state = initState, action) => {
    switch (action.type) {
        case GET_DRIVERS:
            return {
                ...state,
                drivers: action.payload
            }
        case CHANGE_PAGE:
            return {
                ...state,
                page: action.payload
            }
        default:
            return state
    }
}
