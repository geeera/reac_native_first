import React, {useState, useEffect} from 'react'
import {StyleSheet, FlatList, SafeAreaView, View, Button} from 'react-native'
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {changeRound, changeSeason} from '../store/actions/selectActions';
import {filterTableAction} from '../store/actions/filterTableAction';
import {AppPicker} from '../components/AppPicker/AppPicker';
import {AppCart} from '../components/AppCard/AppCard';
import {AppErr} from '../components/AppErr/AppErr';


export const MainScreen = ({navigation}) => {
    const [valuePicker, setValue] = useState('F1')
    const [seasonValuePicker, setSeasonValue] = useState('Select season')
    const [roundValuePicker, setRoundValue] = useState('Select round')

    const dispatch = useDispatch()

    const {seasons, rounds} = useSelector(({selects}) => selects, shallowEqual)
    const {drivers, page} = useSelector(({table}) => table, shallowEqual)
    const {errMessage} = useSelector(({error}) => error, shallowEqual)

    useEffect(() => {
        dispatch(changeSeason())
    }, [dispatch])

    const onPrev = () => {
        dispatch(filterTableAction({season: seasonValuePicker, round: roundValuePicker, page: page - 1}))
    }
    const onNext = () => {
        dispatch(filterTableAction({season: seasonValuePicker, round: roundValuePicker, page: page + 1}))
    }

    return(
        <View>
            <AppErr />
            <View style={styles.wrapper}>
                <View style={styles.pickerWrapper}>
                    <AppPicker
                        firstPicker={{label: 'F1', value: 'F1'}}
                        setValue={setValue}
                        valuePicker={valuePicker}
                    />
                    <AppPicker
                        setValue={setSeasonValue}
                        valuePicker={seasonValuePicker}
                        firstPicker={{label: 'Select season', value: 'Select season'}}
                        pickerItem={seasons}
                        action={changeRound}
                    />
                    <AppPicker
                        setValue={setRoundValue}
                        valuePicker={roundValuePicker}
                        firstPicker={{label: 'Select round', value: 'Select round'}}
                        pickerItem={rounds}
                        action={filterTableAction}
                        actionPayload={{season: seasonValuePicker, round: roundValuePicker}}
                    />
                </View>
                {!errMessage && <SafeAreaView style={styles.list}>
                    <FlatList
                        data={drivers}
                        renderItem={({item}) => <AppCart
                            navigation={navigation}
                            name={item.familyName + ' ' + item.givenName}
                            driverID={item.driverId}
                        />}
                        keyExtractor={item => item.driverId}
                    />
                </SafeAreaView>}
                {!!drivers.length && <View style={styles.buttonWrapper}>
                    <Button title='prev' onPress={onPrev}/>
                    <Button title={`${page}`}/>
                    <Button title='next' onPress={onNext}/>
                </View>}
                <Button
                    title='Посмотреть турнирную таблицу'
                    onPress={() =>
                        navigation.navigate('Driver Standing', {year: seasonValuePicker, round: roundValuePicker})}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 20
    },
    pickerWrapper: {
        position: 'relative',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 25
    },
    list: {
        width: '100%',
        flex: 0
    },
    buttonWrapper: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
