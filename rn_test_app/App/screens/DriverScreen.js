import React, {useEffect} from 'react'
import {StyleSheet, View, Text} from 'react-native'
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {getDriver} from '../store/actions/getDriverAction';

export const DriverScreen = ({route}) => {
    const dispatch = useDispatch()
    const {info} = useSelector(({driver}) => driver, shallowEqual)

    useEffect(() => {
        dispatch(getDriver(route.params.driverID))
    }, [])
    return(
        <View style={styles.wrapper}>
            <View style={styles.block}>
                <Text style={styles.text}>Дата рождения: {info.dateOfBirth}</Text>
                <Text style={styles.text}>Национальность: {info.nationality}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15
    },
    block: {
        padding: 10,
        backgroundColor: '#ff6666',
        borderRadius: 10,
        width: '100%'
    },
    text: {
        marginBottom: 10,
        color: '#fff',
        fontSize: 22
    }
})
