import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'

export const AppCart = ({navigation, driverID, pos, name, wins, age, className}) => {

    if (pos) {
        return(
            <TouchableOpacity onPress={() => driverID && navigation.navigate('Driver', {driverID: driverID})}>
                <View style={styles.cardWrapper}>
                    <Text style={{...styles.cardItem, ...styles.pos, ...className}}>{pos}</Text>
                    <Text style={{...styles.cardItem, ...className}}>|</Text>
                    <Text style={{...styles.cardItem, ...styles.name, ...className}}>{name}</Text>
                    <Text style={{...styles.cardItem, ...className}}>|</Text>
                    <Text style={{...styles.cardItem, ...styles.wins, ...className}}>{wins}</Text>
                    <Text style={{...styles.cardItem, ...className}}>|</Text>
                    <Text style={{...styles.cardItem, ...styles.age, ...className}}>{age}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return(
        <TouchableOpacity onPress={() => navigation.navigate('Driver', {driverID: driverID})}>
            <View style={styles.cardWrapper}>
                <Text style={styles.cardItem}>{name}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    cardWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#ff6666',
        borderRadius: 10,
        marginBottom: 10
    },
    cardItem: {
        fontSize: 20,
        borderStyle: 'solid',
        margin: 10,
        color: '#fff',
        letterSpacing: 1.2,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    pos: {
        width: '10%'
    },
    name: {
        width: 100
    },
    wins: {
        width: '15%'
    },
    age: {
        width: '10%'
    }
})
