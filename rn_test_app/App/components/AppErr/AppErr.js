import React from 'react'
import {Alert, View} from 'react-native';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';

import {clearErrAction} from '../../store/actions/errorActions';

export const AppErr = () => {
    const {errMessage} = useSelector(({error}) => error, shallowEqual)
    const dispatch = useDispatch()
    return (
        <View>
            {!!errMessage && Alert.alert(
                'Error Message',
                errMessage,
                [{
                    text: 'Okay',
                    onPress: () => {
                        dispatch(clearErrAction());
                    }
                }])}
        </View>
    )
}
